package pl.cba.tlos.TLBattleShip.server;


import java.rmi.Naming;

import pl.cba.tlos.TLBattleShip.shared.interfaces.GameServer;

public class Server {
	private static GameServer gameServer;
	public static void main(String[] args) {
		try {
			
			String port = "1099";
			if( args.length == 1)
				port = args[0];

			if(System.getSecurityManager() == null)
				System.setSecurityManager(new SecurityManager());
			
			gameServer = new TLGameServer();
			Naming.rebind( "rmi://localhost:"+port+"/GameServer", gameServer );
		} catch (Exception e) {
			System.err.println(e.getMessage());
			System.exit(1);
			//e.printStackTrace();
		}
	}
}
