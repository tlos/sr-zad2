package pl.cba.tlos.TLBattleShip.server;

import pl.cba.tlos.TLBattleShip.shared.interfaces.User;

public class BotUser implements User {
	private static int counter = 0;
	private static final long serialVersionUID = -5328174193890556309L;
	private String nick = "Bot" + counter++;
	
	public BotUser() {
		
	}
	
	@Override
	public String getNick() {
		return nick;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!obj.getClass().equals(this.getClass()))
			return false;
		return ((BotUser) obj).getNick().equals(getNick());
	}
	@Override
	public int hashCode() {
		return getNick().hashCode();
	}

}
