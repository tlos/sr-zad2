package pl.cba.tlos.TLBattleShip.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.cba.tlos.TLBattleShip.shared.interfaces.Game;
import pl.cba.tlos.TLBattleShip.shared.interfaces.GameBoardListener;
import pl.cba.tlos.TLBattleShip.shared.interfaces.User;
import pl.cba.tlos.TLBattleShip.shared.types.Point;
import pl.cba.tlos.TLBattleShip.shared.types.RandomShipGenerator;
import pl.cba.tlos.TLBattleShip.shared.types.Ship;
import pl.cba.tlos.TLBattleShip.shared.types.UserRejectedException;

public class BotGameBoardListener extends UnicastRemoteObject implements
		GameBoardListener {
	private static final int BOARD_SIZE_X = 10;
	private static final int BOARD_SIZE_Y = 10;
	private Game game;
	private BotThread botThread;
	private User botUser;
	private Random rand = new Random();
	RandomShipGenerator shipsGenerator;
	public BotGameBoardListener(Game game, User botUser) throws RemoteException {
		super(0);
		this.game = game;
		this.botUser = botUser;
		shipsGenerator = new RandomShipGenerator(botUser);
	}

	private List<Ship> createBotShips() {
		List<Ship> ships = new ArrayList<>(3);
		ships.add(shipsGenerator.generateRandomShip());
		ships.add(shipsGenerator.generateRandomShip());
		ships.add(shipsGenerator.generateRandomShip());
		return ships;
	}

	private static final long serialVersionUID = -5216659026008298020L;

	@Override
	public void onGameOver(User winningUser) throws RemoteException {
		botThread.stopThread();
	}

	@Override
	public void onUserMove() throws RemoteException {
		System.out.println("Bot " + botUser.getNick() + " received request to fire");
		botThread.put(new Runnable() {
			@Override
			public void run() {
				makeMove();
			}
		});
	}

	@Override
	public void onHit(Ship hitSip) {}

	@Override
	public void onGameBreak(String msg) throws RemoteException {
		botThread.stopThread();
	}

	
	private void makeMove(){
		int x = rand.nextInt(BOARD_SIZE_X);
		int y = rand.nextInt(BOARD_SIZE_Y);
		Point p = new Point(x,y);
		try {
			Ship s = game.fireAndGetResult(botUser, p);
			System.out.println("Bot " + botUser.getNick() + " fired at point (" + x +","+y+") result " + s );
		} catch (RemoteException | UserRejectedException e) {
			e.printStackTrace();
		}
	}

	public void setShipsAndStart() throws RemoteException {
		try {
			game.setMyShips(botUser, createBotShips());
		} catch (UserRejectedException e) {
			e.printStackTrace();
		}		

		botThread = new BotThread();
	}
	
	
	
}
