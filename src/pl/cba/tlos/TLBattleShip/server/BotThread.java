package pl.cba.tlos.TLBattleShip.server;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class BotThread extends Thread {
	private BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
	
	public BotThread() {
		start();
	}
	public void put(Runnable task) {
		try {
			queue.put(task);
		} catch (InterruptedException e) {
			System.err.println("Bot thread finished");
			e.printStackTrace();
		}
		
	}
	public void stopThread(){
		this.interrupt();
	}
	
	@Override
	public void run() {
		while( !isInterrupted() ){
			try {
				Runnable r = queue.poll(100, TimeUnit.MILLISECONDS);
				if(r != null){
					r.run();
				}
			} catch (InterruptedException e) {
				System.err.println("Bot thread finished");
				e.printStackTrace();
			}
		}
	}
}
