package pl.cba.tlos.TLBattleShip.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import pl.cba.tlos.TLBattleShip.shared.interfaces.Game;
import pl.cba.tlos.TLBattleShip.shared.interfaces.GameBoardListener;
import pl.cba.tlos.TLBattleShip.shared.interfaces.GameServer;
import pl.cba.tlos.TLBattleShip.shared.interfaces.User;
import pl.cba.tlos.TLBattleShip.shared.types.UserRejectedException;

public class TLGameServer extends UnicastRemoteObject implements GameServer {

	private static final long serialVersionUID = -5495883531205976241L;
	private ConcurrentMap<User, GameBoardListener> userListeners;
	private ConcurrentMap<String, TLGame> games;

	private Random rand = new Random();
	
	public TLGameServer() throws RemoteException { 
		super(0);
		userListeners = new ConcurrentHashMap<>();
		games = new ConcurrentHashMap<>();
	}
	

	@Override
	public void registerUser(User user, GameBoardListener userListener)
			throws RemoteException, UserRejectedException {
		if( userListeners.containsKey(user) )
			throw new UserRejectedException("User with nick " + user.getNick() + " exists");
		userListeners.put(user, userListener);
	}

	@Override
	public Game createGame(User user, String gameName) throws RemoteException,
			UserRejectedException {
		
		checkUserAuthorisation(user);
		if( games.containsKey(gameName) )
			throw new UserRejectedException("Game " + gameName + " exists");
		
		TLGame game = new TLGame();
		synchronized (game) {
			game.setUser1(user);
			game.setListener1(userListeners.get(user));
			game.setName(gameName);
			games.put(gameName, game);
			while(game.getSecondUser()==null) // waiting for second player
				try {
					game.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		}
		return game;
	}

	@Override
	public Game joinGame(User user, String gameName) throws RemoteException,
			UserRejectedException {
		checkUserAuthorisation(user);
		TLGame game = games.get(gameName);
		
		if( game == null )
			throw new UserRejectedException("Game " + gameName + " doesn't exit");
		synchronized (game) {
			game.setUser2(user);
			game.setListener2(userListeners.get(user));
			games.remove(gameName);
			game.notifyAll();	
		}
		User startUser = rand.nextBoolean() ? game.getOwner() : game.getSecondUser();  
		userListeners.get(startUser).onUserMove();
		game.currentMove(startUser);
		return game;
	}

	@Override
	public Game joinBot(User user) throws RemoteException,
			UserRejectedException {
		TLGame game = new TLGame();
		User bot = new BotUser();
		BotGameBoardListener botListener = new BotGameBoardListener(game, bot);
		userListeners.put(bot, botListener);
		userListeners.get(user).onUserMove();
		game.setUser1(user)
				.setListener1(userListeners.get(user))
				.setUser2(bot)
				.setListener2(botListener);
		botListener.setShipsAndStart();
		userListeners.get(game.getOwner()).onUserMove();
		game.currentMove(game.getOwner());
		return game;
	}


	@Override
	public List<String> getAvailableGames(User user) throws RemoteException,
			UserRejectedException {
		checkUserAuthorisation(user);
		return new ArrayList<>(games.keySet());
	}



	private void checkUserAuthorisation(User user) throws UserRejectedException{
		if( !userListeners.containsKey(user) )
			throw new UserRejectedException("User with nick " + user.getNick() + " doesn't exist");
//		for( User u : userListeners.keySet() )
//			System.out.println(u.getNick());
	}


	@Override
	public void userLeft(User user) throws RemoteException,
			UserRejectedException {
		checkUserAuthorisation(user);
		userListeners.remove(user);
	}


	@Override
	public void deleteGame(User user, String gameName) throws RemoteException,
			UserRejectedException {
		checkUserAuthorisation(user);
		Game game = games.get(gameName);
		
		if( game == null)
			throw new UserRejectedException("Game doesn't exist");
		
		if( game.getOwner().equals(user) ){
			games.remove(gameName);
		}
		else
			throw new UserRejectedException("Only who created game can delete one");
	}


	
}
