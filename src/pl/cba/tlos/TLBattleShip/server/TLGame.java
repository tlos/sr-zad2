package pl.cba.tlos.TLBattleShip.server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import pl.cba.tlos.TLBattleShip.shared.interfaces.Game;
import pl.cba.tlos.TLBattleShip.shared.interfaces.GameBoardListener;
import pl.cba.tlos.TLBattleShip.shared.interfaces.User;
import pl.cba.tlos.TLBattleShip.shared.types.Point;
import pl.cba.tlos.TLBattleShip.shared.types.Ship;
import pl.cba.tlos.TLBattleShip.shared.types.UserRejectedException;

public class TLGame extends UnicastRemoteObject implements Game, Serializable{

	protected TLGame() throws RemoteException {
		super(0);
	}
	private static final long serialVersionUID = 4631303398125824936L;
	private User user1;
	private GameBoardListener listener1;
	private User user2;
	private GameBoardListener listener2;
	private String name;
	
	private List<Ship> user1Ships; 
	private List<Ship> user2Ships;
	
	private User userWithCurrentMove; 
	private Serializable userWithCurrentMoveSyncObj = new ReentrantLock();
	
	public TLGame setListener1(GameBoardListener listener1) {
		this.listener1 = listener1;
		return this;
	}
	public TLGame setListener2(GameBoardListener listener2) {
		this.listener2 = listener2;
		return this;
	}
	public TLGame setUser1(User user1) {
		this.user1 = user1;
		return this;
	}
	public TLGame setUser2(User user2) {
		this.user2 = user2;
		return this;
	}
	
	
	@Override
	public List<Ship> getMyShips(User user) throws RemoteException,
			UserRejectedException {
		if(user1.equals(user)){
			return user1Ships;
		}else if( user2.equals(user)){
			return user2Ships;
		}else 
			throw new UserRejectedException("User " + user.getNick() + " doesn't exist");
	}

	@Override
	public Ship fireAndGetResult(User user, Point p) throws RemoteException,
			UserRejectedException {

		System.err.println("TLGame:fire:user="+user.getNick());
		checkingIfIsUserMove(user);
		
		Ship shipFlooded = null;
		boolean someoneWin = false;
		if( user.equals(user1) ){
			// we fire at user2 board
			shipFlooded = fire(user2Ships, p);
			someoneWin = checkWin(user2Ships);
			if( someoneWin ){
				listener1.onGameOver(user1);
				listener2.onGameOver(user1);
			}else{
				listener2.onHit(shipFlooded);
				listener2.onUserMove();
				currentMove(user2);
			}
		}
		else if( user.equals(user2) ){
			shipFlooded = fire(user1Ships, p);
			someoneWin = checkWin(user1Ships);
			if( someoneWin ){
				listener1.onGameOver(user2);
				listener2.onGameOver(user2);
			}else{
				listener1.onHit(shipFlooded);
				listener1.onUserMove();
				currentMove(user1);
			}
		}
		else
			throw new UserRejectedException("No such user");
		
		return shipFlooded;
	}
	public void currentMove(User currentUser)  throws RemoteException{
		synchronized (userWithCurrentMoveSyncObj) {
			userWithCurrentMove = currentUser;
		}
	}
	private void checkingIfIsUserMove(User user) throws UserRejectedException {
		if(!user.equals(userWithCurrentMove)){
			throw new UserRejectedException("It's not Your move!!");
		}
	}
	
	public void setName(String gameName) {
		this.name = gameName;		
	}

	private Ship fire( List<Ship> ships, Point point){
		for( Ship s : ships )
			for( Point p : s.getShipPoints() )
				if( p.equals(point)){
					s.sinkPoint(p);
					return s;
				}
		return null;
	}	
	private boolean checkWin(List<Ship> ships){
		for(Ship s : ships)
			if(!s.isSunk())
				return false;
		return true;
	}
	@Override
	public String getName() throws RemoteException {
		return name;
	}
	@Override
	public User getOwner() throws RemoteException {
		return user1;
	}
	@Override
	public User getSecondUser() throws RemoteException {
		return user2;
	}
	@Override
	public void setMyShips(User user, List<Ship> ships) throws RemoteException,
			UserRejectedException {
		if(user1.equals(user)){
			user1Ships = Collections.synchronizedList(new ArrayList<>(ships));
		}else if( user2.equals(user)){
			user2Ships = Collections.synchronizedList(new ArrayList<>(ships));
		}else 
			throw new UserRejectedException("User " + user.getNick() + " doesn't exist");
	}
	@Override
	public void userLeft(User user) throws RemoteException, UserRejectedException {
		if(user1.equals(user)){
			listener2.onGameBreak("User " + user.getNick() + " left game");
		}else if( user2.equals(user)){
			listener1.onGameBreak("User " + user.getNick() + " left game");
		}else 
			throw new UserRejectedException("User " + user.getNick() + " doesn't exist");
	}
}
