package pl.cba.tlos.TLBattleShip.shared.types;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.cba.tlos.TLBattleShip.shared.interfaces.User;

public class RandomShipGenerator {

	private Random rand = new Random(System.currentTimeMillis());
	private List<Ship> sampleShips;
	private User user;
	public RandomShipGenerator(User user) {
		this.user = user;
		creteSampleShips();
	}
	
	public Ship generateRandomShip() {
		return sampleShips.remove( rand.nextInt( sampleShips.size() ) );
	}
	
	private void creteSampleShips() {
		sampleShips = new ArrayList<>(5);
		List<Point> shipPoints = new ArrayList<>(3);
		shipPoints.add(new Point(1, 1));
		shipPoints.add(new Point(2, 1));
		shipPoints.add(new Point(3, 1));
		sampleShips.add(new Ship(user.getNick()+"1", shipPoints));
		

		shipPoints = new ArrayList<>(3);
		shipPoints.add(new Point(1, 4));
		shipPoints.add(new Point(1, 5));
		shipPoints.add(new Point(1, 6));
		sampleShips.add(new Ship(user.getNick()+"2", shipPoints));
		

		shipPoints = new ArrayList<>(3);
		shipPoints.add(new Point(4, 2));
		shipPoints.add(new Point(4, 3));
		shipPoints.add(new Point(4, 4));
		sampleShips.add(new Ship(user.getNick()+"3", shipPoints));
		

		shipPoints = new ArrayList<>(3);
		shipPoints.add(new Point(8, 7));
		shipPoints.add(new Point(8, 8));
		shipPoints.add(new Point(8, 9));
		sampleShips.add(new Ship(user.getNick()+"4", shipPoints));
		

		shipPoints = new ArrayList<>(3);
		shipPoints.add(new Point(3, 7));
		shipPoints.add(new Point(4, 7));
		shipPoints.add(new Point(5, 7));
		sampleShips.add(new Ship(user.getNick()+"5", shipPoints));
	}
	
	
	
}
