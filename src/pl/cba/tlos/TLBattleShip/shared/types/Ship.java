package pl.cba.tlos.TLBattleShip.shared.types;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship implements Serializable{

	private static final long serialVersionUID = 8785862517160546822L;
	private List<Point> shipPoints;
	private List<Point> sunkPoints;
	private boolean isSunk;
	private String name;
	
	public Ship(String name, List<Point> points) {
		this.shipPoints = new ArrayList<>(points);
		this.sunkPoints = new ArrayList<>(shipPoints.size());
		this.isSunk = false;
		this.name = name;
	}
	
	public boolean sinkPoint(Point p){
		if( shipPoints.contains(p) ){
			sunkPoints.add(p);
			checkIfSunkAndSetIsSunk();
			return true;
		}
		else return false;
	}
	
	public boolean sinkPoint(int x, int y){
		Point p = new Point(x,y);
		return sinkPoint(p);
	}

	public List<Point> getSunkPoints() {
		return sunkPoints;
	}
	public boolean isSunk() {
		return isSunk;
	}
	public List<Point> getShipPoints() {
		return shipPoints;
	}
	public Ship getCopyWithOnlySunkPoints(){
		Ship s =  new Ship(name, null);
		s.isSunk = isSunk;
		s.sunkPoints = new ArrayList<>(sunkPoints);
		return s;
	}
	
	private void checkIfSunkAndSetIsSunk() {
		if(shipPoints.size() == sunkPoints.size()){
			isSunk = true;
		}
	}
	@Override
	public String toString() {
		return name;
	}
	@Override
	public boolean equals(Object obj) {
		if(obj==null) return false;
		if(!obj.getClass().equals(this.getClass())) return false;
		return ((Ship)obj).name.equals(name);
	}
	@Override
	public int hashCode() {
		return name.hashCode();
	}
}
