package pl.cba.tlos.TLBattleShip.shared.types;

import java.io.Serializable;

public class Point implements Serializable{
	private static final long serialVersionUID = 4917724119448331676L;
	private final int x;
	private final int y; 
	
	public Point(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	public int getY() {
		return y;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if(obj != null && obj.getClass().equals(this.getClass()) && ((Point)obj).x == x && ((Point)obj).y == y)
			return true;
		return false;
	}
	@Override
	public int hashCode() {
		return x*y+(((x+y)^13)%20001);
	}
}
