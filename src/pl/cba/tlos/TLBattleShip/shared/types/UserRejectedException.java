package pl.cba.tlos.TLBattleShip.shared.types;

public class UserRejectedException extends Exception {
	private static final long serialVersionUID = -5212627371364379610L;
	public UserRejectedException() {
	}
	public UserRejectedException(String msg){
		super(msg);
	}
}
