package pl.cba.tlos.TLBattleShip.shared.interfaces;

import java.io.Serializable;

public interface User extends Serializable{
	public String getNick();
	public boolean equals(Object obj);
	public int hashCode();
}
