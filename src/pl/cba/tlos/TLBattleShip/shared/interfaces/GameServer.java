package pl.cba.tlos.TLBattleShip.shared.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import pl.cba.tlos.TLBattleShip.shared.types.UserRejectedException;

public interface GameServer extends Remote {
	public void registerUser(User user, GameBoardListener userListener) throws RemoteException, UserRejectedException;
	public Game createGame(User user, String gameName) throws RemoteException, UserRejectedException;
	public Game joinGame(User user, String gameName) throws RemoteException, UserRejectedException;
	public Game joinBot( User user ) throws RemoteException,UserRejectedException;
	public List<String> getAvailableGames( User user ) throws RemoteException, UserRejectedException;
	public void userLeft(User user) throws RemoteException, UserRejectedException;
	public void deleteGame(User user, String gameName) throws  RemoteException, UserRejectedException;
}
