package pl.cba.tlos.TLBattleShip.shared.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import pl.cba.tlos.TLBattleShip.shared.types.Point;
import pl.cba.tlos.TLBattleShip.shared.types.Ship;
import pl.cba.tlos.TLBattleShip.shared.types.UserRejectedException;

public interface Game extends Remote{
	public List<Ship> getMyShips(User user) throws RemoteException, UserRejectedException;
	public void setMyShips(User user, List<Ship> ships)  throws RemoteException, UserRejectedException;
	public Ship fireAndGetResult( User user, Point p ) throws RemoteException, UserRejectedException;
	public String getName() throws RemoteException;
	public User getOwner() throws RemoteException;
	public User getSecondUser() throws RemoteException;
	public void currentMove(User currentUser)  throws RemoteException;
	public void userLeft(User user) throws RemoteException, UserRejectedException;
}
