package pl.cba.tlos.TLBattleShip.shared.interfaces;

import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import pl.cba.tlos.TLBattleShip.shared.types.Ship;

public interface GameBoardListener extends Remote, Serializable{
	public void onGameOver( User winningUser ) throws RemoteException;
	public void onUserMove( ) throws RemoteException; 
	public void onHit( Ship hitSip ) throws RemoteException;
	public void onGameBreak( String msg ) throws RemoteException;
}
