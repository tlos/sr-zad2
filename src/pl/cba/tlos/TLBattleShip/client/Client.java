package pl.cba.tlos.TLBattleShip.client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import pl.cba.tlos.TLBattleShip.shared.interfaces.Game;
import pl.cba.tlos.TLBattleShip.shared.interfaces.GameServer;
import pl.cba.tlos.TLBattleShip.shared.interfaces.User;
import pl.cba.tlos.TLBattleShip.shared.types.Point;
import pl.cba.tlos.TLBattleShip.shared.types.Ship;
import pl.cba.tlos.TLBattleShip.shared.types.UserRejectedException;

public class Client {
	private static GameServer gameServer;

	private static User user;
	private static TLGameBoardListener userListener;
	private static Game game;

	public static void main(String[] arg) {
		
		if(System.getSecurityManager() == null)
			System.setSecurityManager(new SecurityManager());
		
		System.out.println("#################");
		try {
			if (arg.length < 3) {
				System.err
						.println("Wrong usage! Should be:\n ... address port nick");
				System.exit(1);
			}
			String nick = arg[2];
			String port = arg[1];
			String address = arg[0];
			gameServer = (GameServer) Naming.lookup("rmi://" + address
					+ ":"+port + "/GameServer");

			user = new TLUser(nick);
			userListener = new TLGameBoardListener(user);
			gameServer.registerUser(user, userListener);

			setAtExit();
			showCommandMenu();
			showInteractiveConsole();
		} catch (MalformedURLException e) {
			// e.printStackTrace();
			System.exit(1);
		} catch (RemoteException e) {
			System.err.println("Error while invoking server's method");
			// e.printStackTrace();
			System.exit(1);
		} catch (NotBoundException e) {
			System.err.println("Server not found");
			// e.printStackTrace();
			System.exit(1);
		} catch (UserRejectedException e) {
			System.err.println(e.getMessage());
			// e.printStackTrace();
			System.exit(1);
		}
	}

	private static void showInteractiveConsole() throws RemoteException {
		while (true) {
			System.out.print(">");
			String cmd = System.console().readLine();
			try {
				game = null;
				if (cmd.startsWith("create") || cmd.startsWith("c")) {
					String gameName = cmd.split(" ")[1];
					System.out
							.println("Creating game and waiting for other user to join");
					game = gameServer.createGame(user, gameName);
					System.out.println("Created game " + game.getName()
							+ ", user joined: "
							+ game.getSecondUser().getNick());
					playGameLoop(user, game.getSecondUser(), game);
				} else if (cmd.startsWith("remove") || cmd.startsWith("r")) {
					String gameName = cmd.split(" ")[1];
					gameServer.deleteGame(user, gameName);
					System.out.println("Removed game " + gameName);
				} else if (cmd.equals("list") || cmd.equals("ls")) {
					List<String> games = gameServer.getAvailableGames(user);
					for (String g : games)
						System.out.println(g);
				} else if (cmd.startsWith("join") || cmd.startsWith("j")) {
					String gameName = cmd.split(" ")[1];
					game = gameServer.joinGame(user, gameName);
					System.out.println("Joined to game " + game.getName()
							+ " with user " + game.getOwner().getNick());
					playGameLoop(user, game.getOwner(), game);

				} else if (cmd.equals("playwithbot") || cmd.equals("b")) {
					game = gameServer.joinBot(user);
					playGameLoop(user, game.getSecondUser(), game);
				} else if (cmd.equals("exit")) {
					gameServer.userLeft(user);
					gameServer = null;
					System.exit(0);
				} else if (cmd.isEmpty()) {

				} else {
					System.out.println("Command not found");
				}
			} catch (ArrayIndexOutOfBoundsException a) {
				System.out.println("You must give an argument for command");
			} catch (UserRejectedException a) {
				System.err.println(a.getMessage());
			}

		}
	}

	private static void playGameLoop(User user, User rival, Game game)
			throws RemoteException, UserRejectedException {
		String line = "";
		TLBoard board = new TLBoard(user, rival);
		List<Ship> userShips = setShips(board);
		while(userShips == null)
			userShips = setShips(board);
		game.setMyShips(user, userShips);
		printGameCommands();
		while (true) {
			try {
				if (userListener.isGameOver() || userListener.isGameBreak()) {
					break;
				}
				List<Ship> myActualShips = game.getMyShips(user);
				// System.out.println("My ships : " + myActualShips);

				board.updateRivalBoard();
				board.updateMyBoard(myActualShips);

				board.printBoardNextToEachOther();
				// board.printMyBoard();
				System.out.println();
				// board.printRivalBoard();

				userListener.waitForMove();

				if (userListener.isGameOver() || userListener.isGameBreak()) {
					break;
				}
				System.out.println("Your move");
				System.out.print(">");

				line = System.console().readLine();

				if (line.startsWith("fire")) {

					String stringPoint[] = line.split(" ");
					int x = (stringPoint[1].charAt(0) - 'a');
					int y = Integer.parseInt(stringPoint[1].substring(1));
					if (stringPoint.length < 2 || x < 0 || y < 0
							|| x >= board.BOARD_SIZE_X
							|| y >= board.BOARD_SIZE_Y) {
						System.err
								.println("you must put command and correct arguments");

					} else {
						// System.out.println("Firing at" + x + "," + y);
						Point point = new Point(x, y);
						Ship result = game.fireAndGetResult(user, point);
						if (result == null)
							board.addFirePoint(point, false);
						else {
							board.addFirePoint(point, true);
							if (result.isSunk()) {
								board.addOrUpdateSunkRivalShip(result);
							}
						}
						userListener.iMakeMove();
					}
				} else if (line.equals("exit")) {
					game.userLeft(user);
					break;
				}

			} catch (UserRejectedException e) {
				System.err.println(e.getMessage());
			} catch (NumberFormatException a) {
				System.err.println("Wrong args format, should by for eg. b2");
			}
		}
		System.out.println("Exiting game");
		showCommandMenu();
	}

	private static List<Ship> setShips(TLBoard board) {
		System.out.println("Please set your 3 ships");
		System.out.println("Just put 3 points to each ship in order x1 y1 x2 y2 x3 y3");
		List<Ship> ships = new ArrayList<>();
		for(int i=1; i<=3; ++i){
			System.out.println("For ship " + i + " >");
			List<Point> points = new ArrayList<>(3);
			String line = System.console().readLine();
			String coordinates [] = line.split(" ");
			if( coordinates.length < 6){
				System.out.println("You put to less coordinates");
				return null;
			}else{
				try{
					points.add(new Point( Integer.parseInt(coordinates[0]),Integer.parseInt(coordinates[1]) ));
					points.add(new Point( Integer.parseInt(coordinates[2]),Integer.parseInt(coordinates[3]) ));
					points.add(new Point( Integer.parseInt(coordinates[4]),Integer.parseInt(coordinates[5]) ));
					for(Point p: points)
						if(!checkPoint(p, board)){
							System.out.println("Point out of range");
							return null;
						}
				}
				catch(NumberFormatException a){
					System.out.println("You put wrond coordinates");
				}
			}
			ships.add(new Ship(user.getNick() + i, points));
		}
		return ships;
		// TODO
//		List<Point> ship1Points = new ArrayList<Point>();
//		ship1Points.add(new Point(1, 2));
//		ship1Points.add(new Point(1, 3));
//		ship1Points.add(new Point(1, 4));
////		Ship ship = new Ship(user.getNick() + 1, ship1Points);
//		List<Ship> ships = new ArrayList<Ship>();
////		ships.add(ship);
////		return ships;
//		RandomShipGenerator rand = new RandomShipGenerator(user);
//		ships.add(rand.generateRandomShip());
//		ships.add(rand.generateRandomShip());
//		ships.add(rand.generateRandomShip());
//		return ships;
	}
	private static boolean checkPoint(Point p, TLBoard board){
		if(p.getX() >= board.BOARD_SIZE_X  || p.getX() < 0 )
			return false;
		if(p.getY() >= board.BOARD_SIZE_Y  || p.getY() < 0 )
			return false;
		return true;
		
	}
	private static void printGameCommands() {
		System.out.println();
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
		System.out.println("Commands");
		System.out.println("fire [a-j][0-9]    eg. fire b2");
		System.out.println("exit");
		System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
	}

	private static void showCommandMenu() {
		System.out.println("*************************");
		System.out.println("What would you like to do ?\ncommands:");
		System.out.println("create __game_name__");
		System.out.println("remove __game_name__");
		System.out.println("list");
		System.out.println("join __game_name__");
		System.out.println("playwithbot");
		System.out.println("exit");
		System.out.println("*************************");
	}

	private static void setAtExit() {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				if (gameServer != null)
					try {
						gameServer.userLeft(user);
					} catch (RemoteException | UserRejectedException e) {
						// e.printStackTrace();
					}
			}
		});
	}

}
