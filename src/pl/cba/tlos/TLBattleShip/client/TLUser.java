package pl.cba.tlos.TLBattleShip.client;

import pl.cba.tlos.TLBattleShip.shared.interfaces.User;

public class TLUser implements User {

	private static final long serialVersionUID = 859000175221792428L;
	private String nick;

	public TLUser(String nick) {
		this.nick = nick;
	}

	@Override
	public String getNick() {
		return nick;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (!obj.getClass().equals(this.getClass()))
			return false;
		return ((TLUser) obj).getNick().equals(getNick());
	}
	@Override
	public int hashCode() {
		return getNick().hashCode();
	}
}
