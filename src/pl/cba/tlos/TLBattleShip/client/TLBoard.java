package pl.cba.tlos.TLBattleShip.client;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import pl.cba.tlos.TLBattleShip.shared.interfaces.User;
import pl.cba.tlos.TLBattleShip.shared.types.Point;
import pl.cba.tlos.TLBattleShip.shared.types.Ship;

public class TLBoard {

	public static final int BOARD_SIZE_X = 10;
	public static final int BOARD_SIZE_Y = 10;
	public static final char SHIP_NOT_SUNK_SIGN = 'S';
	public static final char SHIP_SUNK_SIGN = '$';
	public static final char NO_SHIP_SIGN = '-';
	public static final char HIT_POINT = 'H';
	public static final char MISSED_POINT = 'X';
	

	private char [][] myShipsBoard = new char[BOARD_SIZE_X][BOARD_SIZE_Y];
	private char [][] rivalShipsBoard = new char[BOARD_SIZE_X][BOARD_SIZE_Y];
	
	private Map<Point,Boolean> rivalFirePoint = new HashMap<>();
	private List<Ship> rivalSunkShips = new LinkedList<>();
	
	private User rival;
	private User user;

	
	public TLBoard(User myUser, User rival) {
		this.rival = rival;
		this.user = myUser;
	}
	public void cleanMyBoard(){
		for(int i=0; i<BOARD_SIZE_X; ++i)
			for(int j=0; j<BOARD_SIZE_Y; ++j)
				myShipsBoard[i][j] = NO_SHIP_SIGN;
	}
	public void cleanRivalBoard(){
		for(int i=0; i<BOARD_SIZE_X; ++i)
			for(int j=0; j<BOARD_SIZE_Y; ++j)
				rivalShipsBoard[i][j] = NO_SHIP_SIGN;
	}
	
	public void updateMyBoard(List<Ship> myShips){
		cleanMyBoard();
		for(Ship s: myShips){
			for(Point p: s.getShipPoints()){
				//System.out.println("Putting " + p.getX() + "," +p.getY() + " not sunk ship" );
				myShipsBoard[p.getX()][p.getY()] = SHIP_NOT_SUNK_SIGN;
			}
			for(Point p: s.getSunkPoints()){
				//System.out.println("Putting " + p.getX() + "," +p.getY() + " sunk ship" );
				myShipsBoard[p.getX()][p.getY()] = SHIP_SUNK_SIGN;
			}
		}
	}
	public void addOrUpdateSunkRivalShip(Ship ship){
		if(rivalSunkShips.contains(ship)){
			rivalSunkShips.remove(ship);
		}
		rivalSunkShips.add(ship);
	}
	public void addFirePoint(Point p, boolean hit){
		rivalFirePoint.put(p, hit);
	}
	
	public void updateRivalBoard(){
		cleanRivalBoard();
		
		for(Point p: rivalFirePoint.keySet()){
			boolean hit = rivalFirePoint.get(p);
			rivalShipsBoard[p.getX()][p.getY()] = hit ? HIT_POINT : MISSED_POINT; 
		}
		
		for(Ship s: rivalSunkShips){
			for(Point p: s.getSunkPoints()){
				rivalShipsBoard[p.getX()][p.getY()] = SHIP_SUNK_SIGN;
			}
		}
	}
	public void printMyBoard(){
		printBoard(myShipsBoard, user.getNick() + " Board");
	}
	public void printRivalBoard(){
		printBoard(rivalShipsBoard, rival.getNick() + " Board");
	}
	private void printBoard(char [][]board, String title){
		System.out.println("** " + title + " **");
		System.out.print("  ");
		for(int j=0; j<BOARD_SIZE_Y; ++j)
			System.out.print( j + " ");
		System.out.println();
		for(int i=0; i<BOARD_SIZE_X; ++i){
			System.out.print(((char)('a' +i)) + " ");
			for(int j=0; j<BOARD_SIZE_Y; ++j){ 
				System.out.print(board[i][j]+ " ");
			}
			System.out.println();
		}
	}
	
	public void printBoardNextToEachOther(){
		System.out.print("** " +  user.getNick() + " **");
		System.out.print("                ** " +  rival.getNick() + " **");
		System.out.print("\n  ");
		
		for(int j=0; j<BOARD_SIZE_Y; ++j)
			System.out.print( j + " ");
		System.out.print("       ");
		for(int j=0; j<BOARD_SIZE_Y; ++j)
			System.out.print( j + " ");
		
		System.out.println();
		for(int i=0; i<BOARD_SIZE_X; ++i){
			System.out.print(((char)('a' +i)) + " ");
			for(int j=0; j<BOARD_SIZE_Y; ++j){ 
				System.out.print(myShipsBoard[i][j]+ " ");
			}
			System.out.print("    "+((char)('a' +i)) + "  ");
			for(int j=0; j<BOARD_SIZE_Y; ++j){ 
				System.out.print(rivalShipsBoard[i][j]+ " ");
			}
			System.out.println();
		}
		
	}
	
	
}
