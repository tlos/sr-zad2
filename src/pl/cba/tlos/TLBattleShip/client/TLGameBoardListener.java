package pl.cba.tlos.TLBattleShip.client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import pl.cba.tlos.TLBattleShip.shared.interfaces.GameBoardListener;
import pl.cba.tlos.TLBattleShip.shared.interfaces.User;
import pl.cba.tlos.TLBattleShip.shared.types.Ship;

public class TLGameBoardListener extends UnicastRemoteObject implements
		GameBoardListener {
	private User user;
	private boolean gameOver = false;
	private boolean myMove = false;
	private boolean gameBreak = false;

	public TLGameBoardListener(User user) throws RemoteException {
		super(0);
		this.user = user;
	}

	private static final long serialVersionUID = -4155153411219007365L;

	@Override
	public synchronized  void onGameOver(User winningUser) throws RemoteException {
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		if (user.equals(winningUser))
			System.out.println("You win!!!");
		else
			System.out.println("You lost!!!!!");
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		gameOver = true;
		this.notifyAll();
	}

	@Override
	public synchronized void onUserMove() throws RemoteException {
		
			myMove = true;	
			this.notifyAll();
		
	}

	@Override
	public void onHit(Ship hitShip) throws RemoteException {
		if (hitShip != null) {
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			System.out.println("Your rival hit your ship");
			System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		}
	}

	public synchronized void iMakeMove() {
			myMove = false;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public boolean isMyMove() {
		return myMove;
	}

	public boolean isGameBreak() {
		return gameBreak;
	}

	@Override
	public synchronized void onGameBreak(String msg) throws RemoteException {
		System.out.println(msg);
		gameOver = true;
		gameBreak = true;
		this.notifyAll();
	}

	public synchronized void waitForMove() {
		
			while(!myMove && !gameOver){
				try {
					this.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		
			
		
	}
}
